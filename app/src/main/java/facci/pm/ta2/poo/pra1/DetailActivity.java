package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
        String object_id = getIntent().getStringExtra("object_id");
        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null) {
                    ImageView imgen =(ImageView)findViewById(R.id.thumbnail);
                    imgen.setImageBitmap((Bitmap)object.get("image"));
                    TextView tex1 = (TextView)findViewById(R.id.T1);
                    tex1.setText((String)object.get("name"));


                    TextView tex2 = (TextView)findViewById(R.id.T2);

                    tex2.setText((String)object.get("price") + " \u0024");

                    TextView tex3 = (TextView)findViewById(R.id.T3);
                    tex3.setText((String)object.get("description"));
                } else {
                    // Error
                }
            }


        });


        // FIN - CODE6

    }

}
